<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'slug' => $faker->slug,
        'description' => $faker->paragraph,
        'img' => $faker->imageUrl($width = 640, $height = 480),
        'status' => 1
    ];
});
