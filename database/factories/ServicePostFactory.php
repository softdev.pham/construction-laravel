<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServicePost;
use Faker\Generator as Faker;

$factory->define(ServicePost::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'slug' => $faker->slug,
        'description' => $faker->paragraph,
        'img' => $faker->imageUrl($width = 640, $height = 480),
        'status' => 1,
        'service_id' => function() {
            return factory(App\Service::class)->create()->id;
        },
    ];
});
