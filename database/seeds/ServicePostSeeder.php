<?php

use Illuminate\Database\Seeder;

class ServicePostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ServicePost::class, 10)->create();
    }
}
