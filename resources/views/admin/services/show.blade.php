@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">View Service</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Name: </label>
                                    </div>
                                    <div class="col-md-11">
                                        <span>{{ !empty($service['name']) ? $service['name'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Slug: </label>
                                    </div>
                                    <div class="col-md-11">
                                        <span>{{ !empty($service['slug']) ? $service['slug'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Image: </label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <img style="max-width: 100%;" src="{{ asset('storage/'.$service['img']) }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Status: </label>
                                    </div>
                                    <div class="col-md-11">
                                        <span> {{ !empty($service['status']) ? 'Active' : 'Unactive' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Description: </label>
                                    </div>
                                    <div class="col-md-11">
                                        <span> {!! !empty($service['description']) ? $service['description'] : '' !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
{{--    <script src="{{ asset('admin-lte/js/global.js') }}"></script>--}}
@endsection
