@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">View Service Post</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Name: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($servicePost['name']) ? $servicePost['name'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Slug: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($servicePost['slug']) ? $servicePost['slug'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Name Service: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($servicePost->service) ? $servicePost->service->name : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Image: </label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <img style="max-width: 100%;" src="{{ asset('storage/'.$servicePost['img']) }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Status: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span> {{ !empty($servicePost['status']) ? 'Active' : 'Unactive' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Description: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span> {!! !empty($servicePost['description']) ? $servicePost['description'] : '' !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection
