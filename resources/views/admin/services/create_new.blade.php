@extends('layouts.admin.app_new')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Add Services</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" id="quickForm" action="{{ route('admin.services.store') }}" method="post" enctype="multipart/form-data">
                            <div class="card-body">
                                @include('layouts.errors-and-messages')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Slug</label>
                                    <input type="text" name="slug" class="form-control" id="exampleInputPassword1" placeholder="Enter Slug">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="img" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control select2" name="status" style="width: 100%;">
                                        <option selected="selected">Yes</option>
                                        <option>No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleDescription">Description</label>
                                    <textarea class="textarea" placeholder="Place some text here" name="description" id="description"></textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Create</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('admin-lte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
            // $.validator.setDefaults({
            //     submitHandler: function () {
            //         alert( "Form successful submitted!" );
            //     }
            // });
            // jQuery.validator.addMethod("accept", function (value, element, param)
            // {
            //     return value.match(new RegExp("." + param + "$"));
            // });
            // $('#quickForm').validate({
            //     ignore: [],
            //     rules: {
            //         name: {
            //             required: true,
            //         },
            //         slug: {
            //             required: true,
            //             minlength: 5
            //         },
            //         image: {
            //             accept: "png|jpe?g|tif|gif",
            //             required: false,
            //         },
            //             // description:{
            //             //     ckrequired: true
            //             // }
            //     },
            //     messages: {
            //         name: {
            //             required: "Please enter a email address",
            //             email: "Please enter a vaild email address"
            //         },
            //         slug: {
            //             required: "Please provide a password",
            //             minlength: "Your password must be at least 5 characters long"
            //         },
            //         image: {
            //             accept: "File must be JPG, GIF or PNG",
            //         },
            //         // description:{
            //         //     ckrequired:"Please enter Text",
            //         // }
            //     },
            //     errorElement: 'span',
            //     errorPlacement: function (error, element) {
            //         error.addClass('invalid-feedback');
            //         element.closest('.form-group').append(error);
            //     },
            //     highlight: function (element, errorClass, validClass) {
            //         $(element).addClass('is-invalid');
            //     },
            //     unhighlight: function (element, errorClass, validClass) {
            //         $(element).removeClass('is-invalid');
            //     }
            // });
        });
    </script>
@endsection
