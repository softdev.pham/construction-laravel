@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">View Contact</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Email: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($contact['email']) ? $contact['email'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Phone: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($contact['phone']) ? $contact['phone'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Address: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($contact['address']) ? $contact['address'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Description: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span> {!! !empty($contact['description']) ? $contact['description'] : '' !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection
