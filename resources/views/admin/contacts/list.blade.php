@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                    @include('layouts.errors-and-messages')
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Contacts</h3>
                            @if(count($contacts) < 1)
                            <a href="{{ route('admin.contacts.create') }}" class="btn btn-info float-right"><i class="fas fa-plus"></i> Add item</a>
                            @endif
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 100px">#</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th style="width: 18%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($contacts) > 0)
                                    @foreach($contacts as $key => $contact)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $contact->email }}</td>
                                            <td>{{ $contact->phone }}</td>
                                            <td>{{ $contact->address }}</td>
                                            <td>
                                                <a href="{{ route('admin.contacts.show', ['contact' => $contact->id]) }}"><span class="btn-left badge bg-primary">View</span></a>
                                                <a href="{{ route('admin.contacts.edit', ['contact' => $contact->id]) }}"><span class="btn-left badge bg-success">Edit</span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align: center">
                                            Data not found!
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $contacts->links() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection



