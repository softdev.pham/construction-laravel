@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Add Contacts</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('admin.contacts.store') }}" method="post" enctype="multipart/form-data">
                            <div class="card-body">
                                @include('layouts.errors-and-messages')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName">Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Enter Email" value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputSlug">Phone</label>
                                    <input type="tel" name="phone" maxlength="11" class="form-control" placeholder="Enter Phone" value="{{ old('phone') }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputSlug">Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Enter Address" value="{{ old('address') }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleDescription">Description</label>
                                    <textarea class="textarea" placeholder="Place some text here" name="description" id="description">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Create</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script src="{{ asset('admin-lte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('admin-lte/js/global.js') }}"></script>
@endsection
