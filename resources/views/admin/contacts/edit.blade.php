@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Edit Contact</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        {!! Form::model($contact, [
                            'enctype' => "multipart/form-data",
                            'method' => 'PATCH',
                            'route' => ['admin.contacts.update', $contact->id]
                        ]) !!}
                            <div class="card-body">
                                @include('layouts.errors-and-messages')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName">Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Enter Email" value="{{ !empty($contact['email']) ? Request::old('email', $contact['email']) : '' }}  ">
                                </div>
                                <div class="form-group">
                                    <label for="inputSlug">Phone</label>
                                    <input type="tel" name="phone" maxlength="11" class="form-control" placeholder="Enter Phone" value="{{ !empty($contact['phone']) ? Request::old('phone', $contact['phone']) : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputSlug">Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Enter Address" value="{{ !empty($contact['address']) ? Request::old('address', $contact['address']) : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleDescription">Description</label>
                                    <textarea class="textarea" placeholder="Place some text here" name="description" id="description">{{ !empty($contact['description']) ? Request::old('description', $contact['description']) : '' }}</textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Edit</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script src="{{ asset('admin-lte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('admin-lte/js/global.js') }}"></script>
@endsection
