@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                    @include('layouts.errors-and-messages')
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Simple Full Width Table</h3>
                            <a href="{{ route('admin.project.posts.create') }}" class="btn btn-info float-right"><i class="fas fa-plus"></i> Add item</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 100px">#</th>
                                    <th>Name Projects</th>
                                    <th>Name</th>
                                    <th>Descriptions</th>
                                    <th style="width: 10%">Status</th>
                                    <th style="width: 18%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($project_posts) > 0)
                                    @foreach($project_posts as $key => $project)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $project->project->name }}</td>
                                        <td>{{ $project->name }}</td>
                                        <td>
                                            {!! !empty($project->description) ? trim_word($project->description, 110) : '' !!}
                                        </td>
                                        <td>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox"
                                                       class="custom-control-input"
                                                       id="customSwitch_{{$project['id']}}"
                                                       name="check-switch"
                                                       @if(!empty($project['status'])) checked @endif
                                                       data-id="{{$project['id']}}">
                                                <label class="custom-control-label" for="customSwitch_{{$project['id']}}"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.project.posts.show', ['post' => $project->id]) }}"><span class="btn-left badge bg-primary">View</span></a>
                                            <a href="{{ route('admin.project.posts.edit', ['post' => $project->id]) }}"><span class="btn-left badge bg-success">Edit</span></a>
                                            {{ Form::open(['route' => ['admin.project.posts.destroy', $project->id], 'method' => 'delete']) }}
                                                <button type="submit" class="btn-left badge bg-danger" style="border: 0">Delete</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" style="text-align: center">
                                            Data not found!
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $project_posts->links() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('js')
    <!-- Toastr -->
    <script src="{{ asset('admin-lte/plugins/toastr/toastr.min.js') }}"></script>
    <script>
        setTimeout( function () {
            $(".alert").fadeOut(2000);
        }, 2000)

        $('.custom-switch input[name="check-switch"]').on('change', function(event, state) {
            var dataID = $(this).data('id');
            var status = event.target.checked;

            $.ajax({
                url: '/admin/project/posts/changeStatus/'+dataID,
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: dataID,
                    status: status ? 1 : 0
                },
                dataType: "json",
                success: function (data) {
                    if (data.status) {

                        toastr.success('Change status successfully.')
                    } else {
                        toastr.error('Change status fail.')
                    }
                },
                error: function (error) {
                    toastr.error(error)
                }
            });
        });
    </script>
@endsection

