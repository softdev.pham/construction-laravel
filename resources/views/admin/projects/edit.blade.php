@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Edit Services</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        {!! Form::model($project, [
                            'enctype' => "multipart/form-data",
                            'method' => 'PATCH',
                            'route' => ['admin.projects.update', $project->id]
                        ]) !!}
                            <div class="card-body">
                                @include('layouts.errors-and-messages')
                                <div class="form-group">
                                    <label for="inputName">Name</label>
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Enter Name" value="{{ !empty($project['name']) ? Request::old('name', $project['name']) : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputSlug">Slug</label>
                                    <input type="text" name="slug" class="form-control" id="inputSlug" placeholder="Enter Slug" value="{{ !empty($project['slug']) ? Request::old('slug', $project['slug']) : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="img" class="custom-file-input" id="exampleInputFile" value="">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control select2" name="status" style="width: 100%;">
                                        <option value="1" {{ !empty($project['status']) ? 'selected' : '' }}>Yes</option>
                                        <option value="0" {{ empty($project['status']) ? 'selected' : '' }}>No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleDescription">Description</label>
                                    <textarea class="textarea" placeholder="Place some text here" name="description" id="description">{!! Request::old('description', $project['description']) !!}</textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Edit</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script src="{{ asset('admin-lte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('admin-lte/js/global.js') }}"></script>
@endsection
