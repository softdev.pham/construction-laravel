@extends('layouts.admin.app')

@section('content')
    <?php
        $type_banners = config('constantsystem.type_banners');
    ?>
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                    @include('layouts.errors-and-messages')
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Banners</h3>
                            <a href="{{ route('admin.banners.create') }}" class="btn btn-info float-right"><i class="fas fa-plus"></i> Add item</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 100px">#</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Image</th>
                                    <th style="width: 18%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($banners) > 0)
                                    @foreach($banners as $key => $banner)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $banner->title }}</td>
                                            <td>{{ !empty($type_banners[$banner->type ]) ? $type_banners[$banner->type ] : '' }}</td>
                                            <td width="25%">
                                                    <img style="max-width: 100%;" src="{{ asset('storage/'.$banner->img) }}">
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.banners.show', ['banner' => $banner->id]) }}"><span class="btn-left badge bg-primary">View</span></a>
                                                <a href="{{ route('admin.banners.edit', ['banner' => $banner->id]) }}"><span class="btn-left badge bg-success">Edit</span></a>
                                                {{ Form::open(['route' => ['admin.banners.destroy', $banner->id], 'method' => 'delete']) }}
                                                <button type="submit" class="btn-left badge bg-danger" style="border: 0">Delete</button>
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align: center">
                                            Data not found!
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $banners->links() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection



