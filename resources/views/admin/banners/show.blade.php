@extends('layouts.admin.app')

@section('content')
    <?php
        $type_banners = config('constantsystem.type_banners');
    ?>
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">View Banner</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Title: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($banner['title']) ? $banner['title'] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Type: </label>
                                    </div>
                                    <div class="col-md-10">
                                        <span>{{ !empty($type_banners[$banner->type ]) ? $type_banners[$banner->type ] : '' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Image: </label>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <img style="max-width: 100%;" src="{{ asset('storage/'.$banner['img']) }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection
