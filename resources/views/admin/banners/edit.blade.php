@extends('layouts.admin.app')

@section('content')
    <?php
        $type_banners = config('constantsystem.type_banners');
    ?>
    <!-- Main content -->
    <section class="content content-section">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Edit Contact</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        {!! Form::model($banner, [
                            'enctype' => "multipart/form-data",
                            'method' => 'PATCH',
                            'route' => ['admin.banners.update', $banner->id]
                        ]) !!}
                            <div class="card-body">
                                @include('layouts.errors-and-messages')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName">Title</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Title" value="{{ !empty($banner['title']) ? Request::old('title', $banner['title']) : '' }}  ">
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control select2" name="type" style="width: 100%;">
                                        <option value="" >Select option</option>
                                        @foreach($type_banners as $key => $type_banner)
                                            <option value="{{ $key }}" @if($key == $banner->type) selected @endif>{{ $type_banner }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="img" class="custom-file-input" id="exampleInputFile" value="{{ old('img') }}">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Edit</button>
                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script src="{{ asset('admin-lte/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('admin-lte/js/global.js') }}"></script>
@endsection
