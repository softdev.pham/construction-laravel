@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection



@section('content')
    @include('layouts.front.banner')
    <div class="hompage-service">
        @include('front.home.service')
    </div>
{{--    @include('front.home.about')--}}
{{--    @include('front.home.process')--}}
    @include('front.home.our-project')
@endsection

