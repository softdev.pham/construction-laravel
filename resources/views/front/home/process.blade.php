<section>
    <div class="hero-slide owl-carousel site-blocks-cover">
        <div class="site-section services-1-wrap">
            <div class="container">
                <div class="row mb-5 justify-content-center text-center">
                    <div class="col-lg-5">
                        <h3 class="section-subtitle">What We Do</h3>
                        <h2 class="section-title mb-4 text-black">We Are <strong>Leading Industry</strong> of Engineering. We Love What We Do</h2>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-lg-3 col-md-6">
                        <div class="service-1">
                            <span class="number">01</span>
                            <div class="service-1-icon">
                                <span class="flaticon-engineer"></span>
                            </div>
                            <div class="service-1-content">
                                <h3 class="service-heading">Professional Team</h3>
                                <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-1">
                            <span class="number">02</span>
                            <div class="service-1-icon">
                                <span class="flaticon-compass"></span>
                            </div>
                            <div class="service-1-content">
                                <h3 class="service-heading">Great Ideas</h3>
                                <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-1">
                            <span class="number">03</span>
                            <div class="service-1-icon">
                                <span class="flaticon-oil-platform"></span>
                            </div>
                            <div class="service-1-content">
                                <h3 class="service-heading">Quality Building</h3>
                                <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-1">
                            <span class="number">04</span>
                            <div class="service-1-icon">
                                <span class="flaticon-crane"></span>
                            </div>
                            <div class="service-1-content">
                                <h3 class="service-heading">Quality Works</h3>
                                <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END services -->
</section>
