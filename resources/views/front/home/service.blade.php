<section>
    <div class="site-section services-1-wrap">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-5">
                    <h3 class="section-subtitle">Chúng tôi làm gì?</h3>
                    <h2 class="section-title mb-4 text-black">Chúng tôi là <strong>dịch vụ</strong> vận tải. Chúng tôi mang đến những dịch vụ tốt nhất cho bạn</h2>
                </div>
            </div>
            <div class="row">
                @foreach($services as $key => $service)
                    <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
                        <div class="blog-entry">
                            <a href="dich-vu/<?= $service->slug ?>" class="img-link small-img-link">
                                <img src="{{ asset('storage/'.$service->img) }}" alt="Image" class="img-fluid">
                            </a>
                            <div class="blog-entry-contents">
                                <h3><a href="dich-vu/<?= $service->slug ?>"><?= $service->name ?></a></h3>
                                <div class="meta description-info">{{ substr(strip_tags($service->description),0,50) . "..."  }}</div>
                                <div><a class="btn btn-primary btn-sm px-5 rounded-0" href="/dich-vu/<?= $service->slug ?>">Xem thêm</a></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row text-center mt-5 pagination-content">
                <div class="col-12">
                    {{ $services->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- END services -->
</section>
