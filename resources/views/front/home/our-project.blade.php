<section>
    <div class="site-section block-3">
        <div class="container">

            <div class="mb-5">
                <h3 class="section-subtitle">Sản phẩm của chúng tối</h3>
                <h2 class="section-title mb-4"><strong>Một số sản phẩm gần nhất</strong></h2>
            </div>

            <div class="projects-carousel-wrap">
                <div class="owl-carousel owl-slide-3">
                    <div class="project-item">
                        <div class="project-item-contents">
                            <a href="#">
                                <span class="project-item-category">Factory</span>
                                <h2 class="project-item-title">
                                    Building Refinery
                                </h2>
                            </a>
                        </div>
                        <img src="images/works_1.jpg" alt="Image" class="img-fluid">
                    </div>
                    <div class="project-item">
                        <div class="project-item-contents">
                            <a href="#">
                                <span class="project-item-category">Factory</span>
                                <h2 class="project-item-title">
                                    Building Refinery
                                </h2>
                            </a>
                        </div>
                        <img src="images/works_2.jpg" alt="Image" class="img-fluid">
                    </div>
                    <div class="project-item">
                        <div class="project-item-contents">
                            <a href="#">
                                <span class="project-item-category">Factory</span>
                                <h2 class="project-item-title">
                                    Building Refinery
                                </h2>
                            </a>
                        </div>
                        <img src="images/works_3.jpg" alt="Image" class="img-fluid">
                    </div>
                    <div class="project-item">
                        <div class="project-item-contents">
                            <a href="#">
                                <span class="project-item-category">Factory</span>
                                <h2 class="project-item-title">
                                    Building Refinery
                                </h2>
                            </a>
                        </div>
                        <img src="images/works_4.jpg" alt="Image" class="img-fluid">
                    </div>


                </div>
            </div>

        </div>
    </div>
    <!-- END Our project -->
</section>
