@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    <div class="intro-section site-blocks-cover innerpage" style="background-image: url('images/hero_1.jpg');">
        <div class="container">
            <div class="row align-items-center text-center border">
                <div class="col-lg-12 mt-5" data-aos="fade-up">
                    <h1>Sản phẩm</h1>
                    <p class="text-white text-center">
                        <a href="{{ route('home') }}">Trang chủ </a>
                        <span class="mx-2">/</span>
                        <span>Sản phẩm </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('front.home.our-project')
@endsection
