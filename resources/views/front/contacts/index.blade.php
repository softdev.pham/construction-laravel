@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    <div class="intro-section site-blocks-cover innerpage" style="background-image: url('images/hero_1.jpg');">
        <div class="container">
            <div class="row align-items-center text-center border">
                <div class="col-lg-12 mt-5" data-aos="fade-up">
                    <h1>Liên hệ</h1>
                    <p class="text-white text-center">
                        <a href="{{ route('home') }}">Trang chủ </a>
                        <span class="mx-2">/</span>
                        <span>Liên hệ</span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="fname">Họ</label>
                    <input type="text" id="fname" class="form-control form-control-lg">
                </div>
                <div class="col-md-6 form-group">
                    <label for="lname">Tên</label>
                    <input type="text" id="lname" class="form-control form-control-lg">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="eaddress">Email</label>
                    <input type="text" id="eaddress" class="form-control form-control-lg">
                </div>
                <div class="col-md-6 form-group">
                    <label for="tel">Số Điện Thoại</label>
                    <input type="tel" maxlength="11" class="form-control form-control-lg"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="message">Nội dung</label>
                    <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <input type="submit" value="Gửi" class="btn btn-primary rounded-0 px-3 px-5">
                </div>
            </div>
        </div>
    </div>
@endsection
