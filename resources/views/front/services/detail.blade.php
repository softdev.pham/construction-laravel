@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    <div class="intro-section site-blocks-cover innerpage" style="background-image: url({{ !empty($service->img) ? '/storage/'.$service->img : 'images/hero_1.jpg'}});">
        <div class="container">
            <div class="row align-items-center text-center border">
                <div class="col-lg-12 mt-5" data-aos="fade-up">
                    <h1>{{ $service->name }}</h1>
                    <p class="text-white text-center">
                        <a href="{{ route('home') }}">Trang chủ </a>
                        <span class="mx-2">/</span>
                        <span>Dịch vụ </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row">
{{--                <div class="col-md-7 mb-4">--}}
{{--                    <img src="{{ asset('storage/'.$service->img) }}" alt="Image" class="img-fluid">--}}
{{--                </div>--}}
                <div class="col-md-8 ml-auto blog-content">
                    <h2 class="text-black mb-4">{{ $service->name }}</h2>
                    <div>
                        {!! $service->description !!}
                    </div>
                </div>
                <div class="col-md-4 sidebar">
                    <div class="">
                        <form action="#" class="search-form">
                            <div class="form-group">
                                <span class="icon fa fa-search"></span>
                                <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                            </div>
                        </form>
                    </div>
                    <div class="sidebar-box">
                        <div class="categories">
                            <h3 class="text-black">Danh sách bài viết</h3>
                            @foreach($service->service_posts as $key => $service_post)
                                <li><a href="{{ $service->slug .'/'. $service_post->slug }}">{{ $service_post->name }}</a></li>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
