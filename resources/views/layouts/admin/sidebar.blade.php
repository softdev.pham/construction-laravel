<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('admin-lte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin-lte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link @if(request()->segment(1) == 'admin' && empty(request()->segment(2))) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview <?= !empty(request()->segment(2) && (request()->segment(2) == 'services' || request()->segment(2) == 'service')) ? 'menu-open' : '' ?>">
                    <a href="{{ route('admin.services.index') }}" class="nav-link @if(request()->segment(2) == 'services' || request()->segment(2) == 'service') active @endif">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Sevices
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview <?= (!empty(request()->segment(2)) && request()->segment(2) == 'services') ? 'menu-open' : '' ?>">
                        <li class="nav-item">
                            <a href="{{ route('admin.services.index') }}" class="nav-link @if(request()->segment(2) == 'services') active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Services</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.service.posts.index') }}" class="nav-link @if(request()->segment(2) == 'service' && request()->segment(3) == 'posts') active @endif">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Sevice Posts</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview <?= (!empty(request()->segment(2)) && (request()->segment(2) == 'projects' || request()->segment(2) == 'project')) ? 'menu-open' : '' ?>">
                    <a href="{{ route('admin.projects.index') }}" class="nav-link @if(request()->segment(2) == 'projects' || request()->segment(2) == 'project') active @endif">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Projects
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: <?= (!empty(request()->segment(2)) && (request()->segment(2) == 'projects' || request()->segment(2) == 'project')) ? 'block' : 'none' ?>">
                        <li class="nav-item">
                            <a href="{{ route('admin.projects.index') }}" class="nav-link @if(request()->segment(2) == 'projects') active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Projects</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.project.posts.index') }}" class="nav-link @if(request()->segment(2) == 'project' && request()->segment(3) == 'posts') active @endif">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Project Posts</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.projects.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            About us
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.contacts.index') }}" class="nav-link @if(request()->segment(1) == 'admin' && request()->segment(2) == 'contacts') active @endif">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Contact
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.banners.index') }}" class="nav-link @if(request()->segment(1) == 'admin' && request()->segment(2) == 'banners') active @endif">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Banner
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin.services.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-tree"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
