<section>
    <!-- Header top  -->
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <!-- Header bottom -->
    <div class="header-top bg-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6 col-lg-3">
                    <a href="index.html">
                        <img src="{{ asset('/images/logo.png') }}" alt="Image" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-3 d-none d-lg-block">

                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-placeholder text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">{{ !empty($contact['address']) ? $contact['address'] : '' }}</span>
                            <span class="caption-text">Địa chỉ </span>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 d-none d-lg-block">
                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-call text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">{{ !empty($contact['phone']) ? $contact['phone'] : '' }}</span>
                            <span class="caption-text">Số điện thoại </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 d-none d-lg-block">
                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-email text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">{{ !empty($contact['email']) ? $contact['email'] : '' }}</span>
                            <span class="caption-text">Email </span>
                        </div>
                    </div>
                </div>

                <div class="col-6 d-block d-lg-none text-right">
                    <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                            class="icon-menu h3"></span></a>
                </div>
            </div>
        </div>




        <div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner">

            <div class="container">
                <div class="d-flex align-items-center">

                    <div class="mr-auto">
                        <nav class="site-navigation position-relative text-right" role="navigation">
                            <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">
                                <li class="{{ (request()->is('/')) ? 'active' : '' }}">
                                    <a href="/" class="nav-link text-left">Trang chủ </a>
                                </li>
                                <li class="{{ request()->segment(1) == 'dich-vu' ? 'active' : '' }}">
                                    <a href="{{ route('services.index') }}" class="nav-link text-left">Dịch vụ </a>
                                </li>
                                <li class="{{ request()->segment(1) == 'san-pham' ? 'active' : '' }}">
                                    <a href="{{ route('project') }}" class="nav-link text-left">Sản phẩm </a>
                                </li>
                                <li class="{{ request()->segment(1) == 'gioi-thieu' ? 'active' : '' }}">
                                    <a href="{{ route('about') }}" class="nav-link text-left">Giới thiệu </a>
                                </li>
                                <li class="{{ request()->segment(1) == 'lien-he' ? 'active' : '' }}">
                                    <a href="{{ route('contact') }}" class="nav-link text-left">Liên hệ </a>
                                </li>
                            </ul>                                                                                                                                                                                                                                                                                          </ul>
                        </nav>

                    </div>

                </div>
            </div>

        </div>

    </div>
</section>
