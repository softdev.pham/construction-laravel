<section>
    <div class="hero-slide owl-carousel site-blocks-cover">
        @foreach($banners as $key => $banner)
            <div class="intro-section" style="background-image: url({{ !empty($banner->img) ? 'storage/'.$banner->img : 'images/hero_1.jpg'}})">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
                            <span class="d-block"></span>
                            <h1>{{ $banner->title ?? '' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <!-- END slider -->
</section>
