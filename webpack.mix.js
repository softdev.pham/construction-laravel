const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .sass('resources/sass/app.scss', 'public/css')
    .styles(
        [
            'node_modules/bootstrap/dist/css/bootstrap.css',
            'node_modules/font-awesome/css/font-awesome.css',
            'node_modules/ionicons/dist/css/ionicons.css',
            'node_modules/select2/dist/css/select2.css',
            'public/admin-lte/css/AdminLTE.min.css',
            'public/admin-lte/css/skins/skin-purple.min.css',
            'node_modules/datatables/media/css/jquery.dataTables.css',
            'public/css/admin.css'
        ],
        'public/css/admin.min.css'
    )
    .scripts(
        [
            'public/js/jquery-3.3.1.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/select2/dist/js/select2.js',
            'node_modules/datatables/media/js/jquery.dataTables.js',
            'public/admin-lte/js/app.js'
        ],
        'public/js/admin.min.js'
    )
    .copyDirectory('node_modules/datatables/media/images', 'public/images')
    .copyDirectory('node_modules/font-awesome/fonts', 'public/fonts')
    .copyDirectory('public/admin-lte/img', 'public/img');
    // .styles(
    // [
    //     'resources/fonts/icomoon/style.css',
    //     'resources/fonts/flaticon/font/flaticon.css',
    //     'resources/css/css/aos.css',
    //     'resources/css/jquery.mb.YTPlayer.min.css',
    //     'resources/css/style.css',
    //     'resources/css/bootstrap-datepicker.css',
    //     'resources/css/jquery.fancybox.min.css',
    //     'resources/css/owl.theme.default.min.css',
    //     'resources/css/owl.carousel.min.css',
    //     'resources/css/jquery-ui.css',
    //     'resources/css/bootstrap.min.css'
    //
    // ],
    // 'public/css/style.min.css'
    // )
    // .scripts(
    //     [
    //         'resources/js/jquery-3.3.1.min.js',
    //         'resources/js/jquery-migrate-3.0.1.min.js',
    //         'resources/js/jquery-ui.js',
    //         'resources/js/popper.min.js',
    //         'resources/js/bootstrap.min.js',
    //         'resources/js/owl.carousel.min.js',
    //         'resources/js/jquery.stellar.min.js',
    //         'resources/js/jquery.countdown.min.js',
    //         'resources/js/bootstrap-datepicker.min.js',
    //         'resources/js/jquery.easing.1.3.js',
    //         'resources/js/aos.js',
    //         'resources/js/jquery.fancybox.min.js',
    //         'resources/js/jquery.sticky.js',
    //         'resources/js/jquery.mb.YTPlayer.min.js'
    //     ],
    //     'public/js/front.min.js'
    // )
    // .scripts(
    //     [
    //         'resources/js/main.js',
    //     ],
    //     'public/js/main.min.js'
    // )
    // .copyDirectory('resources/fonts', 'public/fonts')
    // .copyDirectory('resources/images', 'public/images')
    // .copyDirectory('resources/scss', 'public/scss');
