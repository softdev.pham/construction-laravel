<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPost extends Model
{
    protected $table = "project_posts";

    protected $fillable = [
        'project_id',
        'name',
        'slug',
        'description',
        'img',
        'status'
    ];

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id', 'id');
    }
}
