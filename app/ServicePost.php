<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePost extends Model
{
    protected $table = "service_posts";

    protected $fillable = [
        'service_id',
        'name',
        'slug',
        'description',
        'img',
        'status'
    ];

    public function service()
    {
        return $this->belongsTo('App\Service');
    }
}
