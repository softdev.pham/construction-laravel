<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";

    protected $fillable = [
        'name',
        'slug',
        'description',
        'img',
        'status'
    ];

    public function service_posts()
    {
        return $this->hasMany('App\ProjectPost', 'project_id', 'id');
    }
}
