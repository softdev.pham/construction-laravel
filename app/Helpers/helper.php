<?php

    function imploadValue($types){
        $strTypes = implode(",", $types);
        return $strTypes;
    }

    function explodeValue($types){
        $strTypes = explode(",", $types);
        return $strTypes;
    }

    function random_code(){

        return rand(1111, 9999);
    }

    function remove_special_char($text) {

        $t = $text;

        $specChars = array(
            ' ' => '-',    '!' => '',    '"' => '',
            '#' => '',    '$' => '',    '%' => '',
            '&amp;' => '',    '\'' => '',   '(' => '',
            ')' => '',    '*' => '',    '+' => '',
            ',' => '',    '₹' => '',    '.' => '',
            '/-' => '',    ':' => '',    ';' => '',
            '<' => '',    '=' => '',    '>' => '',
            '?' => '',    '@' => '',    '[' => '',
            '\\' => '',   ']' => '',    '^' => '',
            '_' => '',    '`' => '',    '{' => '',
            '|' => '',    '}' => '',    '~' => '',
            '-----' => '-',    '----' => '-',    '---' => '-',
            '/' => '',    '--' => '-',   '/_' => '-',

        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }

    function truncate($string,$length=100,$dots="&hellip;") {
        $string = trim($string);


        if(strlen($string) > $length) {
            $string = wordwrap($string, $length);
            $string = explode("\n", $string, 2);
            $string = $string[0] . $dots;
        }

        return $string;

    }

    function trim_word($text, $length, $startPoint=0, $allowedTags=""){
        $text = html_entity_decode(htmlspecialchars_decode($text));
        $text = strip_tags($text, $allowedTags);
        return $text = substr($text, $startPoint, $length).'...';
    }
