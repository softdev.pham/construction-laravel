<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "services";

    protected $fillable = [
        'name',
        'slug',
        'description',
        'img',
        'status'
    ];

    public function service_posts()
    {
        return $this->hasMany('App\ServicePost');
    }
}
