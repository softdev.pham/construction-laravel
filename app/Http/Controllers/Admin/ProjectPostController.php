<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectPost;
use App\Service;
use App\ServicePost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProjectPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project_posts = ProjectPost::with('project')->latest()->paginate(15);

        return view('admin.projects.posts.list', compact('project_posts'))
            ->with('i', (request()->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::all();
        return view('admin.projects.posts.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_datas = $request->all();
        $request->validate([
            'name'         => 'required',
            'slug'         => 'required',
            'img'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);
        $cover = $request->file('img');
        $extension = $cover->getClientOriginalExtension();
        $image_name = $cover->getFilename().'_'.Str::random(20);
        $image_url = 'images/projects/posts/'.$image_name.'.'.$extension;
        Storage::disk('public')->put($image_url,  File::get($cover));

        unset($input_datas['img']);
        unset($input_datas['_token']);
        $input_datas = array_merge($input_datas, ['img' => $image_url]);

        ProjectPost::firstOrCreate($input_datas);

        return redirect()->route('admin.project.posts.index')->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectPost  $projectPost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projectPost = ProjectPost::find($id);
        return view('admin.projects.posts.show', compact('projectPost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectPost  $projectPost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::all();
        $projectPost = ProjectPost::find($id);
        return view('admin.projects.posts.edit', compact('projectPost', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectPost  $projectPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_datas = $request->all();
        $project_post = ProjectPost::findOrFail($id);
        $cover = $request->file('img');
        if (!empty($cover)) {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'img'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'description' => 'required',
            ]);
            $extension = $cover->getClientOriginalExtension();
            $image_name = $cover->getFilename().'_'.Str::random(20);
            $image_url = 'images/projects/posts/'.$image_name.'.'.$extension;
            Storage::disk('public')->put($image_url,  File::get($cover));
            unset($input_datas['img']);
            $input_datas = array_merge($input_datas, ['img' => $image_url]);

        } else {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'description' => 'required',
            ]);
        }
        $project_post->update($input_datas);

        return redirect()->route('admin.project.posts.index')->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectPost  $projectPost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projectPost = ProjectPost::findOrFail($id);
        $projectPost->delete();
        return redirect()->route('admin.project.posts.index')->with('message', ' Data is successfully deleted');
    }

    public function changeStatus(Request $request, $id)
    {
        $project_post = ProjectPost::findOrFail($id);
        $status = $request->status;

        if ($project_post->update(['status' => $status])) {
            return response()->json([
                'status' =>true,
                'message'=>'Update status successfully.'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message'=>'Update status fail.'
            ]);
        }
    }
}
