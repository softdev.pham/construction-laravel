<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ServicePost;
use App\Service;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ServicePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_posts = ServicePost::with('service')->latest()->paginate(15);

        return view('admin.services.posts.list', compact('service_posts'))
            ->with('i', (request()->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        return view('admin.services.posts.create', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_datas = $request->all();
        $request->validate([
            'name'         => 'required',
            'slug'         => 'required',
            'img'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);
        $cover = $request->file('img');
        $extension = $cover->getClientOriginalExtension();
        $image_name = $cover->getFilename().'_'.Str::random(20);
        $image_url = 'images/services/posts/'.$image_name.'.'.$extension;
        Storage::disk('public')->put($image_url,  File::get($cover));

        unset($input_datas['img']);
        unset($input_datas['_token']);
        $input_datas = array_merge($input_datas, ['img' => $image_url]);

        ServicePost::firstOrCreate($input_datas);

        return redirect()->route('admin.service.posts.index')->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicePost  $servicePost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servicePost = ServicePost::find($id);
        return view('admin.services.posts.show', compact('servicePost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicePost  $servicePost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = Service::all();
        $servicePost = ServicePost::find($id);
        return view('admin.services.posts.edit', compact('servicePost', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicePost  $servicePost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_datas = $request->all();
        $service_post = ServicePost::findOrFail($id);
        $cover = $request->file('img');
        if (!empty($cover)) {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'img'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'description' => 'required',
            ]);
            $extension = $cover->getClientOriginalExtension();
            $image_name = $cover->getFilename().'_'.Str::random(20);
            $image_url = 'images/services/posts/'.$image_name.'.'.$extension;
            Storage::disk('public')->put($image_url,  File::get($cover));
            unset($input_datas['img']);
            $input_datas = array_merge($input_datas, ['img' => $image_url]);

        } else {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'description' => 'required',
            ]);
        }
        $service_post->update($input_datas);

        return redirect()->route('admin.service.posts.index')->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicePost  $servicePost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicePost = ServicePost::findOrFail($id);
        $servicePost->delete();
        return redirect()->route('admin.service.posts.index')->with('message', ' Data is successfully deleted');
    }

    public function changeStatus(Request $request, $id)
    {
        $service_post = ServicePost::findOrFail($id);
        $status = $request->status;

        if ($service_post->update(['status' => $status])) {
            return response()->json([
                'status' =>true,
                'message'=>'Update status successfully.'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message'=>'Update status fail.'
            ]);
        }
    }
}
