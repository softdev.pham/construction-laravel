<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::latest()->paginate(15);

        return view('admin.projects.list', compact('projects'))
            ->with('i', (request()->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_datas = $request->all();
        $request->validate([
            'name'         => 'required',
            'slug'         => 'required',
            'img'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);
        $cover = $request->file('img');
        $extension = $cover->getClientOriginalExtension();
        $image_name = $cover->getFilename().'_'.Str::random(20);
        $image_url = 'images/projects/'.$image_name.'.'.$extension;
        Storage::disk('public')->put($image_url,  File::get($cover));

        unset($input_datas['img']);
        unset($input_datas['_token']);
        $input_datas = array_merge($input_datas, ['img' => $image_url]);

        Project::firstOrCreate($input_datas);

        return redirect()->route('admin.projects.index')->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $input_datas = $request->all();
        $cover = $request->file('img');
        if (!empty($cover)) {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'img'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'description' => 'required',
            ]);
            $extension = $cover->getClientOriginalExtension();
            $image_name = $cover->getFilename().'_'.Str::random(20);
            $image_url = 'images/projects/'.$image_name.'.'.$extension;
            Storage::disk('public')->put($image_url,  File::get($cover));
            unset($input_datas['img']);
            $input_datas = array_merge($input_datas, ['img' => $image_url]);

        } else {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'description' => 'required',
            ]);
        }

        $project->update($input_datas);

        return redirect()->route('admin.projects.index')->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('admin.projects.index')->with('message', ' Data is successfully deleted');
    }

    public function changeStatus(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $status = $request->status;

        if ($project->update(['status' => $status])) {
            return response()->json([
                'status' =>true,
                'message'=>'Update status successfully.'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message'=>'Update status fail.'
            ]);
        }
    }
}
