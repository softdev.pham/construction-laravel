<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Cassandra\Session;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Service;


class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::latest()->paginate(15);

        return view('admin.services.list', compact('services'))
                ->with('i', (request()->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_datas = $request->all();
        $request->validate([
            'name'         => 'required',
            'slug'         => 'required',
            'img'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);
        $cover = $request->file('img');
        $extension = $cover->getClientOriginalExtension();
        $image_name = $cover->getFilename().'_'.Str::random(20);
        $image_url = 'images/services/'.$image_name.'.'.$extension;
        Storage::disk('public')->put($image_url,  File::get($cover));
//        $success = $img->move($upload_path,$image_full_name);

        unset($input_datas['img']);
        unset($input_datas['_token']);
        $input_datas = array_merge($input_datas, ['img' => $image_url]);

        Service::firstOrCreate($input_datas);

        return redirect()->route('admin.services.index')->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_datas = $request->all();
        $service = Service::findOrFail($id);
        $cover = $request->file('img');
        if (!empty($cover)) {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'img'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'description' => 'required',
            ]);
            $extension = $cover->getClientOriginalExtension();
            $image_name = $cover->getFilename().'_'.Str::random(20);
            $image_url = 'images/services/'.$image_name.'.'.$extension;
            Storage::disk('public')->put($image_url,  File::get($cover));
            unset($input_datas['img']);
            $input_datas = array_merge($input_datas, ['img' => $image_url]);

        } else {
            $request->validate([
                'name'         => 'required',
                'slug'         => 'required',
                'description' => 'required',
            ]);
        }

        $service->update($input_datas);

        return redirect()->route('admin.services.index')->with('message', 'Update successful');
    }

    /**Update
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
        return redirect()->route('admin.services.index')->with('message', ' Data is successfully deleted');
    }

    public function changeStatus(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $status = $request->status;

        if ($service->update(['status' => $status])) {
            return response()->json([
                'status' =>true,
                'message'=>'Update status successfully.'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message'=>'Update status fail.'
            ]);
        }
    }
}
