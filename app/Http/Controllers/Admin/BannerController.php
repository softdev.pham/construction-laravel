<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::latest()->paginate(15);

        return view('admin.banners.list', compact('banners'))
            ->with('i', (request()->input('page', 1) - 1) * 15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type_banners = Config::get('constantsystem.type_banners');
        return view('admin.banners.create', compact('type_banners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_datas = $request->all();
        $request->validate([
            'img'          => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $cover = $request->file('img');
        $extension = $cover->getClientOriginalExtension();
        $image_name = $cover->getFilename().'_'.Str::random(20);
        $image_url = 'images/banners/'.$image_name.'.'.$extension;
        Storage::disk('public')->put($image_url,  File::get($cover));

        unset($input_datas['img']);
        unset($input_datas['_token']);
        $input_datas = array_merge($input_datas, ['img' => $image_url]);

        Banner::firstOrCreate($input_datas);

        return redirect()->route('admin.banners.index')->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Banner::find($id);
        return view('admin.banners.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('admin.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input_datas = $request->all();
        $service = Banner::findOrFail($id);
        $cover = $request->file('img');
        if (!empty($cover)) {
            $request->validate([
                'img'          => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $extension = $cover->getClientOriginalExtension();
            $image_name = $cover->getFilename().'_'.Str::random(20);
            $image_url = 'images/banners/'.$image_name.'.'.$extension;
            Storage::disk('public')->put($image_url,  File::get($cover));
            unset($input_datas['img']);
            $input_datas = array_merge($input_datas, ['img' => $image_url]);

        }

        $service->update($input_datas);

        return redirect()->route('admin.banners.index')->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try{
            $service = Banner::findOrFail($id);
            if ($service->delete()) {
                DB::commit();
                return redirect()->route('admin.banners.index')->with('message', ' Data is successfully deleted');
            }
        } catch (\Exception $e) {

            DB::rollBack();
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }

    }
}
