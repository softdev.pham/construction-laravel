<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\ServicePost;
use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::where('status', 1)->latest()->paginate(6);
        return view('front.services.index', compact('services'));
    }

    public function detail($slug)
    {
        $service = Service::with(['service_posts' => function ($q) {
            $q->where('status', 1);
        }])->where('slug', $slug)->first();
        return view('front.services.detail', compact('service'));
    }

    public function post_detail($slug_service, $slug_post)
    {
        $service_post = ServicePost::with(['service'])->where('slug', $slug_post)->first();
        return view('front.services.posts.detail', compact('service_post'));
    }
}
