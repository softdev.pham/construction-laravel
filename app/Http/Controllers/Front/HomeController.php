<?php

namespace App\Http\Controllers\Front;

use App\Banner;
use App\Contact;
use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $services = Service::where('status', 1)->paginate(3);
        $banners = Banner::where('type', 1)->get();

        return view('front.index', compact('services', 'banners'));
    }
}
