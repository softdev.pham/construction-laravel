<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ADMIN AREA
Route::namespace('Admin')->group( function () {
    Route::get('admin/login', 'LoginController@login_get')->name('admin.login');
//    Route::post('admin/login', 'LoginControoler@login_post')->name('admin.login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/linkstorage', function () {
        Artisan::call('storage:link');
    });
    Route::namespace('Admin')->group( function () {
        // Dashboard
        Route::get('/', 'DashboardController@index')->name('dashboard');

        //Admin services
        Route::resource('services', 'ServiceController');
        Route::post('services/changeStatus/{id}', 'ServiceController@changeStatus')->name('services.changeStatus');

        Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
            Route::resource('posts', 'ServicePostController');
            Route::post('posts/changeStatus/{id}', 'ServicePostController@changeStatus')->name('posts.changeStatus');
        });

        //Admin Projects
        Route::resource('projects', 'ProjectController');
        Route::post('projects/changeStatus/{id}', 'ProjectController@changeStatus')->name('projects.changeStatus');

        Route::group(['prefix' => 'project', 'as' => 'project.'], function () {
            Route::resource('posts', 'ProjectPostController');
            Route::post('posts/changeStatus/{id}', 'ProjectPostController@changeStatus')->name('posts.changeStatus');
        });

        Route::resources([
            'contacts' => 'ContactController',
            'banners' => 'BannerController',
        ]);

   });
});




//Route::namespace('Auth')->group(function () {
//    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
//    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
//    Route::get('logout', 'LoginController@logout');
//});

Route::namespace('Front')->group( function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/dich-vu', 'ServiceController@index')->name('services.index');
    Route::get('/dich-vu/{slug}', 'ServiceController@detail')->name('services.detail');
    Route::get('/dich-vu/{slug_service}/{slug_post}', 'ServiceController@post_detail')->name('services.post.detail');
    Route::get('/san-pham', 'OurProjectController@index')->name('project');
    Route::get('/lien-he', 'ContactController@index')->name('contact');
    Route::get('/gioi-thieu', 'AboutController@index')->name('about');
});
